#!/usr/bin/python
import numpy as np
import copy
import sys
import os
import matplotlib.pyplot as plt

pseudopot_home = os.environ['PSEUDOPOT_HOME']


#####################
# To do list:
# 	
#
#
#
#
#
####################
class Ions():
	def read_ionpos(self,fname):
		self.name=fname[:-7]
		f=open(fname,'r')
		comments = f.readline()
		lines = []
		for line in f.readlines():
			lines.append(line.split()[1:5])
		f.close()
		self.ions=[[line[0]]+[float(x) for x in line[1:4]] for line in lines]
		
	def read_mol(self,fname):
		f=open(fname,'r')
		self.name=f.readline().split()[0]
		self.creator = f.readline().split('\n')[0]
		self.comments = f.readline().split('\n')[0]
		N = int(f.readline().split()[0])
		self.ions=[]
		for i in range(N):
			line=f.readline().split()
			self.ions.append([line[3].upper()]+[float(x)/0.5291772109 for x in line[0:3]])
	
	def molecule_boundaries(self):
		xs = [ion[1] for ion in self.ions]
		ys = [ion[2] for ion in self.ions]
		zs = [ion[3] for ion in self.ions]
		self.x_bounds = [min(xs),max(xs)]
		self.y_bounds = [min(ys),max(ys)]
		self.z_bounds = [min(zs),max(zs)]
		self.size_x = self.x_bounds[1]-self.x_bounds[0]
		self.size_y = self.y_bounds[1]-self.y_bounds[0]
		self.size_z = self.z_bounds[1]-self.z_bounds[0]
		
	def find_center_of_mass(self):
		total_coords = [0.0,0.0,0.0]
		for line in self.ions:
			for i in range(3):
				total_coords[i]=total_coords[i]+line[i+1]
		self.c_o_mass=[total_coord/len(self.ions) for total_coord in total_coords]
	
	def write2xyz(self):
		lines=self.ions[:]
		for i in range(len(lines)):
			lines[i][0] = lines[i][0][0].upper()
			#convert to angstrom
			lines[i][1] = lines[i][1]*0.5291772109
			lines[i][2] = lines[i][2]*0.5291772109
			lines[i][3] = lines[i][3]*0.5291772109
			#-------
			lines[i] = '\t'.join([str(x) for x in lines[i][0:4]])

		f=open(self.name+'.xyz','w')
		f.write('%d\n\n' % len(lines))
		f.write('\n'.join(lines))
		f.close()
	
	def make_rotation_matrix(self,ion1,ion2):
		"""
		Takes the indexes of two ions in the ionlist (self.ions)
		and writes the rotation matrix that rotates the molecule
		such that the line that goes through these ions is parallel
		to z axis.
		"""
		r1=np.array(self.ions[ion1][1:4])
		r2=np.array(self.ions[ion2][1:4])

		v = (r1-r2)/np.linalg.norm(r1-r2)
		v_x=v[0]
		v_y=v[1]
		v_z=v[2]
		alpha = np.arccos(  v_x / (v_x**2+v_y**2)**0.5   )
		beta = np.arctan(  (v_x**2+v_y**2)**0.5 / v_z   )

		cosa = (  v_x / (v_x**2+v_y**2)**0.5   )
		sina = (  v_y / (v_x**2+v_y**2)**0.5   )

		a = (v_x**2+v_y**2)**0.5

		cosb = -v_z/(a**2+v_z**2)**0.5
		sinb = a/(a**2+v_z**2)**0.5

		R_z = np.array([[cosa,sina,0.],[-sina,cosa,0.],[0.,0.,1.]])
		R_y = np.array([[cosb,0.,sinb],[0.,1.,0.],[-sinb,0.,cosb]])

		self.rot_mat = np.dot(R_y,R_z)
	
	def rotate(self):
		for i in range(len(self.ions)):
			pos = np.array(self.ions[i][1:])
			newpos = np.dot(self.rot_mat,pos)
			self.ions[i][1]=newpos[0]
			self.ions[i][2]=newpos[1]
			self.ions[i][3]=newpos[2]
		self.molecule_boundaries()
		self.find_center_of_mass()
	
	def translate(self,r):
		for i in range(len(self.ions)):
			pos = np.array(self.ions[i][1:])
			newpos = pos + np.array(r)
			self.ions[i][1]=newpos[0]
			self.ions[i][2]=newpos[1]
			self.ions[i][3]=newpos[2]
		self.molecule_boundaries()
		self.find_center_of_mass()
			
	def __add__(self,other):
		total = copy.deepcopy(self)
		total.name = self.name + '_' +other.name
		total.ions = total.ions + other.ions
		total.molecule_boundaries()
		return total
	
	def writelattice2xyz(self,a,b,c,n_a=5,n_b=5,n_c=1):
		lattice = copy.deepcopy(self)
		lat_sites=[]
		for i in range(n_a):
			for j in range(n_b):
				for k in range(n_c):
					if i==0 and j==0 and k==0:
						continue
					lat_sites.append(a*i+b*j+c*k)
		
		for nuc in self.ions:
			for lat_site in lat_sites:
				x = lat_site[0]
				y = lat_site[1]
				z = lat_site[2]
				lattice.ions.append([nuc[0],nuc[1]+x,nuc[2]+y,nuc[3]+z])
		lattice.name=lattice.name+'_lat'
		lattice.write2xyz()
		del(lattice)
	
	def write2in(self,a,b,c,symmetries=False,ionic_min=False,pspot_uspp=True):
		if pspot_uspp:
			pseudopot_directory = pseudopot_home + "/GBRV/"
			pseudopot_suffixe = ''
			pseudopot_type = '.uspp'
		else:
			pseudopot_directory = pseudopot_home + "/fhi/"
			pseudopot_suffixe = ''
			pseudopot_type = '.fhi'
		
		lattice_str = """lattice \\
%f        %f          %f \\
%f        %f          %f \\
%f        %f          %f 
coords-type cartesian
"""%(a[0],b[0],c[0],a[1],b[1],c[1],a[2],b[2],c[2])
		
		self.writelattice2xyz(a,b,c)
		
		minimize_str="""
electronic-minimize \\
nIterations 50 \\
energyDiffThreshold 1e-8

#debug  EigsFillings
dump End IonicPositions
dump-name %s.$VAR  #Filename pattern for outputs
#dump End Fillings
dump End Ecomponents  #Output energy components at the end
#dump End Forces

"""%self.name
		
		if not symmetries:
			minimize_str=minimize_str+'\nsymmetries none\n'
		if ionic_min:
			minimize_str=minimize_str+'\nionic-minimize \\\n\tnIterations %d \n'%ionic_min
		
		set_of_ions=set([ion[0] for ion in self.ions])
		ion_species_str = '\n'.join(['ion-species '+pseudopot_directory+ion.lower()+pseudopot_suffixe\
						+ pseudopot_type for ion in set_of_ions])+'\n\n'
		
		row = self.ions[0]
		first_ion_str = 'ion %s %f %f %f 0\n'%(row[0]+pseudopot_suffixe,row[1],row[2],row[3]) 
		ion_positions_str = first_ion_str + '\n'.join(['ion %s %f %f %f 1'\
									%(row[0]+pseudopot_suffixe,row[1],row[2],row[3]) \
															for row in self.ions[1:]])
		in_file = self.name+'.in'
		f=open(in_file,'w')
		f.write(lattice_str+ion_species_str+ion_positions_str+minimize_str)
		f.close()
		return in_file
		

		
	def __init__(self,fname):
		self.fname = fname
		try:
			if fname[-4:]=='.mol':
				print('reading the mol file')
				self.read_mol(fname)
			elif fname[-7:]=='.ionpos':
				self.read_ionpos(fname)
			else:
				print('Couldn\'t recognize the file format')
			print('File was successfully read')
			
		except:
			print('Can\'t read %s'%fname)
			print(sys.exc_info())
			return
		self.molecule_boundaries()
		self.find_center_of_mass()	
	
	
		
	
				
	

	

	##def find_inert_tens(self):
		##inert_tens = np.zeros((3,3))
		##xcm = self.c_o_mass[0]
		##ycm = self.c_o_mass[1]
		##zcm = self.c_o_mass[2]
		##for line in self.ions:
			##x = line[1]
			##y = line[2]
			##z = line[3]
			##inert_tens[0][0]=(y-ycm)**2+(z-zcm)**2
			##inert_tens[1][1]=(x-xcm)**2+(z-zcm)**2
			##inert_tens[2][2]=(x-xcm)**2+(y-ycm)**2
			##inert_tens[0][1]=-(x-xcm)*(y-ycm)
			##inert_tens[0][2]=-(x-xcm)*(z-zcm)
			##inert_tens[1][2]=-(y-ycm)*(z-zcm)
			##inert_tens[1][0]=inert_tens[0][1]
			##inert_tens[2][0]=inert_tens[0][2]
			##inert_tens[2][1]=inert_tens[1][2]
		##self.inert_tens = inert_tens

	##def find_rotation_matrix(self):
		##w,v = np.linalg.eig(self.inert_tens)
		##self.w=w
		##self.v=v
		##self.rot_mat=np.array([[1.0,0.,0.],[0.,0.,1.],[0.0,1.,0.]])

def energy_convergence(fname,iter_string='IonicMinimize',to_be_shown=False):
	"""
	Reads fname.out file and returns total energies as a list for each iteration.
	"""	
	f=open(fname+'.out','r')
	output_string = f.read()
	iteration_lines = [line for line in output_string.split('\n') if iter_string in line]
	energies = np.array([float(u.split()[4]) for u in iteration_lines if 'Iter' in u])
	if to_be_shown:
		plt.plot(energies)
		plt.show()
	return energies
	
def readenergy(fname):
	"""
	Reads the file 'fname.Ecomponents' and returns the energy components as 
	a list of floats.
	"""
	f=open(fname+'.Ecomponents','r')
	energies=[]
	for row in f.readlines():
		try:
			energies.append(float(row.split('=')[1]))
		except:
			continue
	f.close()
	return energies

def run_in_local(fname):
	commandstr = 'jdftx -i %s.in -o %s.out'%(fname,fname)
	os.system(commandstr)

def run_in_slurm(fname):
	f=open(fname+'.sh','w')
	f.write(\
"""#! /bin/bash
#SBATCH -J PA6
/home/yalcin/JDFT/build/jdftx -i %s.in -o %s.out"""%(fname,fname))
	f.close()
	commandstr = 'sbatch %s.sh'%fname
	slurm_answer_pipe = os.popen(commandstr)
	slurm_answer_str = slurm_answer_pipe.read()
	slurm_answer = slurm_answer_str.split()
	slurm_answer_pipe.close()
	if slurm_answer[0]=='Submitted':
		return int(slurm_answer[3])
	else:
		print(slurm_answer)
		return None

def run_in_slurm_gpu(fname):
        f=open(fname+'.sh','w')
        f.write(\
"""#! /bin/bash
#SBATCH -J PA6 -gres=gpu -n 1 -N 1
/home/christine/JDFTX/stableVersion/jdftx/build/jdftx_gpu -i %s.in -o %s.out"""%(fname,fname))
        f.close()
        commandstr = 'sbatch %s.sh'%fname
        slurm_answer_pipe = os.popen(commandstr)
        slurm_answer_str = slurm_answer_pipe.read()
        slurm_answer = slurm_answer_str.split()
        slurm_answer_pipe.close()
        if slurm_answer[0]=='Submitted':
                return int(slurm_answer[3])
        else:
                print(slurm_answer)
                return None

def energy_vs_z(fname,z_mean,z_spread_percent=0.2,number_of_samples=10,rotate_ions_tuple=None):
	molecule = Ions(fname)
	if rotate_ions_tuple is not None:
		ion1,ion2 = rotate_ions_tuple
		molecule.make_rotation_matrix(ion1,ion2)
		molecule.rotate()
	r = [-molecule.c_o_mass[0],-molecule.c_o_mass[1],-molecule.z_bounds[0]]
	molecule.translate(r)
	z_list = np.linspace(z_mean/2.0*(1.0-z_spread_percent),z_mean/2.0*(1.0+z_spread_percent),number_of_samples)
	energies = []
	for z in z_list:
		molecule.translate([0.0,0.0,z])
		#parity transform
		ref_molecule = copy.deepcopy(molecule)
		ref_molecule.rot_mat = np.array([[-1.,0.0,0.0],[0.0,-1.,0.],[0.,0.,-1.]])
		ref_molecule.rotate()
		unit_bilayer = molecule + ref_molecule
		unit_bilayer.name = unit_bilayer.name+'_z_sep_%.2f'%(2*z)
		
		#translate it back to original for iterations.
		molecule.translate([0.0,0.0,-z])
		
		print(unit_bilayer.size_x,' ',unit_bilayer.size_y,' ',unit_bilayer.size_z)
		a=np.array([unit_bilayer.size_x,0.0,0.0])
		b=np.array([0.0,unit_bilayer.size_y,0.0])
		c=np.array([0.0,0.0,unit_bilayer.size_z])
		
		in_file = unit_bilayer.write2in(a,b,c)
		run_in_file(unit_bilayer.name)
		energy = readenergy(unit_bilayer.name)[-1]
		energies.append(energy)
		print(z,'\t',energy)
		unit_bilayer.write2xyz()
		
	plt.plot(z_list*2,energies,'.')
	plt.show()
	
def energy_vs_a(fname,x,z=3.65,rotate_ions_tuple=None,run=True):
	molecule = Ions(fname)
	if rotate_ions_tuple is not None:
		ion1,ion2 = rotate_ions_tuple
		molecule.make_rotation_matrix(ion1,ion2)
		molecule.rotate()
	r = [-molecule.c_o_mass[0],-molecule.c_o_mass[1],-molecule.z_bounds[0]+z/2.0]
	molecule.translate(r)	
	ref_molecule = copy.deepcopy(molecule)
	ref_molecule.rot_mat = np.array([[-1.,0.0,0.0],[0.0,-1.,0.],[0.,0.,-1.]])
	ref_molecule.rotate()
	
	unit_bilayer = molecule + ref_molecule
	unit_bilayer.name = unit_bilayer.name+'_z_sep_%.2f_a_%.2f'%(z,x)
	
	a=np.array([x,0.0,0.0])
	b=np.array([x*np.cos(np.pi/3),x*np.sin(np.pi/3),0.0])
	c=np.array([0.0,0.0,unit_bilayer.size_z+10.0])
	
	in_file = unit_bilayer.write2in(a,b,c)
	if run:
		run_in_file(unit_bilayer.name)
		energy = readenergy(unit_bilayer.name)[-1]
		print('z= ', z,'\ta= ',a,'\tenergy= ',energy)
	unit_bilayer.write2xyz()
	


def main():
	energy_convergence('output',to_be_shown=True)


if __name__=='__main__':
	main()
