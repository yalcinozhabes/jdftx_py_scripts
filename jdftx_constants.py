""" jdftx constants file, v5

    Contains commonly used physical constants in jdftx output analysis

    Deniz Gunceler, April 2012 """


## UNIT CONVERSIONS ##
bohr_as_angstrom = 0.529177211
angstrom_as_bohr = 1.88972612
hartree_as_eV = 27.21139
eV_as_hartree = 0.03674932
GPa_as_au = 3.398931e-5
hartreeOverA3_as_GPa = 4359.744

elements = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba']

class element_dict(dict):
	def __getitem__(self,key):
		try:
			return dict.__getitem__(self,key)
		except KeyError:
			if len(key)>2:
				key=key[:2].title()
				if not (key in elements):
					raise KeyError('Element is not introduced.')
				try:
					return dict.__getitem__(self,key)
				except KeyError:
					try:
						return dict.__getitem__(self,key[0])
					except KeyError:
						raise
## ATOMIC NUMBERS ##
atomic_numbers = element_dict()

for counter in range(len(elements)):
    atomic_numbers[elements[counter]] = counter+1
    atomic_numbers[counter+1] = elements[counter]
del counter


## VAN DER WAALS RADII ##
vanDerWaalsRadii = element_dict({'H': 1.20, 'Li': 1.82, 'B': 1.80, 'C': 1.70, 'O': 1.52, 'S': 1.80, 'F': 1.35, 'Si': 2.10, 'Cl': 1.80, 'Sr': 2.55, 'Ti': 2.15, 'Cu': 1.40, 'N': 1.55, 'P':1.80, 'Au':1.66, 'Pt': 1.75,'Mn': 2.05, 'Ni': 2.00, 'Bi': 2.07, 'Sn': 2.25, 'Mo': 2.10}) # In angstroms

for key in list(vanDerWaalsRadii.keys()):# Converts to Bohr
    vanDerWaalsRadii[key] = vanDerWaalsRadii[key]*angstrom_as_bohr



## CHEMISTRY COLORS ## Yalcin's note: Jmol probably: http://jmol.sourceforge.net/jscolors/
colorMap = element_dict({'H': (1.0,1.0,1.0),
            'Li': (0.8,0.50196078,1.0),
            'Na': (0.6705882352941176, 0.3607843137254902, 0.9490196078431372),
            'K' : (0.5607843137254902, 0.25098039215686274, 0.8313725490196079),
            'B' : (1., 0.7098, 0.7098),
            'O' : (1.0, 0.05098039, 0.05098039),
            'Si': (0.94118, 0.78431, 0.62745),
            'Sr': (0,1,0),
            'Ti': (0.5, 0.5, 0.5),
            'C' : (0.5647058823529412,0.5647058823529412,0.5647058823529412),
            'N' : (0.1,1,0.1),
            'P' : (1.0,0.50196078,0.0),
            'S' : (1,1,0),
            'F' : (0.56471, 0.87843, 0.31373),
            'Cl': (0.12157, 0.94118, 0.12157),
            'Br': (0.6509803921568628, 0.1607843137254902, 0.1607843137254902),
			'I' : (0.5803921568627451, 0.0, 0.5803921568627451),
            'Cu': (1, 0.40784313725490196, 0.12156862745098039),
            'N' : (0.181154, 0.30192334, 0.93596235),
            'Au': (1.0, 0.8196078431372549, 0.13725490196078433),
            'Pt': (0.81568627,  0.81568627,  0.87843137),
            'Mn': (0.61176, 0.47843, 0.78039),
            'Ni': (0.3137254901960784, 0.8156862745098039, 0.3137254901960784),
            'Bi': (0.61961, 0.30980, 0.70980),
            'Sn': (0.40000, 0.50196, 0.50196),
            'Mo': (0.32941, 0.70980, 0.70980)})

## JDFT 1 PARAMETERS ##
n_crit = 4.73e-3/(angstrom_as_bohr**3) # Critical radius for JDFT 1, per Bohr^-3
gamma = 0.6




## CORE CHARGES ##
core_charge = {'Li': 2, 'O': 2, 'S': 10, 'F': 2, 'Sr': 28, 'Ti': 10, 'C': 2, 'H': 0, 'Cl': 2}

### Yalcin's Contribution
##Ionic Radii ##
ionicRadii=element_dict({'H': 0.5,
            'Li': 0.9,
            'Be': 0.6,
            'B': 0.41,
            'C': 0.9,
            'O': 1.26,
            'F': 1.19,
            'Na': 1.16,
            'Mg': 0.86,
            'Al': 0.68,
            'S': 1.70,
            'Cl': 1.67,
            'K':1.52,
            'Ca':1.14,
            'Ga': 0.76,
            'Se': 1.84,
            'Br': 1.82,
            'Rb':1.66,
            'Sr': 1.32,
            'In': 0.94,
            'Te': 2.07,
            'I': 2.06}) # all in angstrom
            
  
for key in list(ionicRadii.keys()):# Converts to Bohr
    ionicRadii[key] = ionicRadii[key]*angstrom_as_bohr

# Copy the missing elements from each dictionary to the other:
for key in list(vanDerWaalsRadii.keys()):
	if not (key in list(ionicRadii.keys())):
		ionicRadii[key] = vanDerWaalsRadii[key] 
for key in list(ionicRadii.keys()):
	if not (key in list(vanDerWaalsRadii.keys())):
		vanDerWaalsRadii[key] = ionicRadii[key]
# End of copying
del(key)
